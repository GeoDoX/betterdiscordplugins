//META{"name":"betterTransparencyPatcher"}*//

var betterTransparencyPatcher = function() {};

betterTransparencyPatcher.prototype.name = 'Better Transparency Patcher';
betterTransparencyPatcher.prototype.isPatched = false;

betterTransparencyPatcher.prototype.start = function()
{
    this.patch();
}

betterTransparencyPatcher.prototype.stop = function()
{
    this.patch();
}

betterTransparencyPatcher.prototype.patch = function()
{
    var fs = require("fs");
    var file = require('electron').remote.app.getAppPath() + "/index.js";

    var line1Pattern = "transparent: ";
    var line1Original = "transparent: false";
    var line1New = "transparent: true";

    var line2Pattern = "var app = _electron2.default.app";
    var line2ToAdd = "app.commandLine.appendSwitch('enable-transparent-visuals');";

    var isCurrentlyPatched = this.isPatched;
    var successful = false;

    var log = this.log;

    fs.readFile(file, "utf8", function (errRead, data)
    {
        if (errRead)
        {
            console.error("An error occurred while attempting to read file: ", errRead);
            return;
        }

        // Split the contents into lines
        var sData = data.split('\r\n');

        // Iterate over lines
        for (var i = 0; i < sData.length; i++)
        {
            var currentLine = sData[i];

            // If the current line isn't a line we need to (un)patch, continue
            if (!currentLine.includes(line1Pattern) && !currentLine.includes(line2Pattern) && !currentLine.includes(line2ToAdd))
                continue;

            // Check for first pattern
            if (currentLine.includes(line1Pattern))
            {
                // Check to see if it's not already patched
                if (currentLine.includes(line1Original))
                {
                    // If it should be patched, patch it
                    if (!isCurrentlyPatched)
                        sData[i] = currentLine.replace(line1Original, line1New);
                }
                // Check to see if it's already patched
                else if (currentLine.includes(line1New))
                {
                    // If it shouldn't be patched, unpatch it
                    if (isCurrentlyPatched)
                        sData[i] = currentLine.replace(line1New, line1Original);
                }
            }

            // Check for second pattern
            if (currentLine.includes(line2Pattern))
            {
                var tempLine;
                var tempLineIndex = i;

                // As of 0.0.297 there's some extra stuff we need to skip
                while (!(tempLine = sData[tempLineIndex]).includes(";"))
                    tempLineIndex++;

                // Check to see if it's not already patched
                if (!sData[tempLineIndex + 1].includes(line2ToAdd))
                {
                    // If it should be patched, patch it
                    if (!isCurrentlyPatched)
                        sData.splice(tempLineIndex + 1, 0, line2ToAdd);
                }
                // Check to see if it's already patched
                else
                {
                    // If it shouldn't be patched, unpatch it
                    if (isCurrentlyPatched)
                        sData.splice(i, 1);
                }
            }
        }

        // Join each of the lines again
        var jData = sData.join('\r\n');

        // Write over the file
        fs.writeFile(file, jData, function(errWrite)
        {
            if(errWrite)
            {
                console.error("An error occurred while attempting to write file: ", errWrite);
                return;
            }
        });

        successful = true;
    });

    if (successful)
        this.isPatched = !isCurrentlyPatched;
}

betterTransparencyPatcher.prototype.checkPatched = function()
{
    var fs = require("fs");
    var file = require('electron').remote.app.getAppPath() + "/index.js";
    var line1Patched = "transparent: true";
    var line2Patched = "app.commandLine.appendSwitch('enable-transparent-visuals');";

    fs.readFile(file, "utf8", function (errRead, data)
    {
        if (errRead)
        {
            console.error("An error occurred while attempting to read file: ", errRead);
            return;
        }

        var patched = true;

        if (!data.includes(line1Patched))
            patched = false;
        
        if (!data.includes(line2Patched))
            patched = false;

        if (patched)
            alert("Better Discord is patched with Better Transparency Patcher.");
        else
            alert("Better Discord is NOT patched with Better Transparency Patcher.");
    });
}

betterTransparencyPatcher.prototype.getSettingsPanel = function()
{
    var settingsPanelHTML = '<button onClick="betterTransparencyPatcher.prototype.checkPatched()">Check Patched</button>';

    return settingsPanelHTML;
}

betterTransparencyPatcher.prototype.getName = function()
{
    return this.name;
}

betterTransparencyPatcher.prototype.getDescription = function()
{
    return 'Patches your Discord installation to allow for transparent themes. Patches/Unpatches according to whether the plugin is enabled/disabled respectively.<br><br>Based off of Holly\'s Transparency Patcher.';
}

betterTransparencyPatcher.prototype.getVersion = function()
{
    return '1.0.0';
}

betterTransparencyPatcher.prototype.getAuthor = function()
{
    return '<a href="https://bitbucket.org/GeoDoX/">GeoDoX</a>';
}

betterTransparencyPatcher.prototype.load = function()	{}
betterTransparencyPatcher.prototype.unload = function() {}
betterTransparencyPatcher.prototype.onMessage = function() {}
betterTransparencyPatcher.prototype.onSwitch = function() {}
betterTransparencyPatcher.prototype.observer = function() {}

betterTransparencyPatcher.prototype.log = (msg) =>
{
    console.log("%c[" + this.name + "]%c " + msg, "color: #DABEEF; font-weight: bold;", "");
}
