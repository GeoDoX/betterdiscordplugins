//META{"name":"goodnightServers"}*//

var goodnightServers = function() {};

goodnightServers.prototype.name = "Goodnight Servers";
goodnightServers.prototype.namespace = "goodnightServers";

goodnightServers.prototype.guildsWrapper = null;
goodnightServers.prototype.guildsWrapperSW = null;
goodnightServers.prototype.active = false;

// Variables
goodnightServers.prototype.keyCode = 9;              // Default: 9 (Tab)
goodnightServers.prototype.transitionTime = 0.2;     // Default: 0.2s
goodnightServers.prototype.shouldToggle = false;     // Default: false

// Storage Keys
goodnightServers.prototype.keyCodeKey = "keyCode";
goodnightServers.prototype.transitionTimeKey = "transitionTime";
goodnightServers.prototype.shouldToggleKey = "shouldToggle";

goodnightServers.prototype.start = function()
{
    this.guildsWrapper = $(".guilds-wrapper")[0];

    this.guildsWrapperSW = this.guildsWrapper.querySelector('.scroller-wrap');

    if (this.guildsWrapperSW)
    {
        this.guildsWrapperSW.style.width = "inherit";
        this.guildsWrapperSW.firstChild.style.overflow = "hidden";
    }

    if (this.guildsWrapper)
    {
        $(document).on(this.namespaceEvent("keydown", this.namespace), (e) => this.keyDown(e));
        $(document).on(this.namespaceEvent("keyup", this.namespace), (e) => this.keyUp(e));
        
        this.hide();
    }
}

goodnightServers.prototype.stop = function()
{
    if (this.guildsWrapper)
    {
        $(document).off(this.namespaceEvent("keydown", this.namespace), (e) => this.keyDown(e));
        $(document).off(this.namespaceEvent("keyup", this.namespace), (e) => this.keyUp(e));

        this.show();
    }
}

goodnightServers.prototype.hide = function()
{
    this.guildsWrapper.style.transition = "width " + this.transitionTime + "s";
    this.guildsWrapper.style.width = "0";

    this.active = false;
}

goodnightServers.prototype.show = function()
{
    this.guildsWrapper.style.transition = "width " + this.transitionTime + "s";
    this.guildsWrapper.style.width = "70px";

    this.active = true;
}

goodnightServers.prototype.applySettings = function()
{
    this.keyCode = parseInt($("#keyCodeInput").val());
    bdPluginStorage.set(this.namespace, this.keyCodeKey, this.keyCode);

    this.transitionTime = parseFloat($("#transitionTimeInput").val());
    bdPluginStorage.set(this.namespace, this.transitionTimeKey, this.transitionTime);

    this.shouldToggle = this.parseBoolean($("#shouldToggleInput").val(), "Toggle", "Held");
    bdPluginStorage.set(this.namespace, this.shouldToggleKey, this.shouldToggle);
}

goodnightServers.prototype.keyDown = function(e)
{
    if (this.shouldToggle)
        return;

    var kc = e.which || e.keyCode;

    if (kc === this.keyCode)
        this.show();
}

goodnightServers.prototype.keyUp = function(e)
{
    var kc = e.which || e.keyCode;

    if (kc === this.keyCode)
    {
        if (!this.shouldToggle)
            this.hide();
        else
        {
            if (this.active)
                this.hide();
            else
                this.show();
        }
    }
}

goodnightServers.prototype.getSettingsPanel = function()
{
    var settingsPanelHTML =
    '<div id="goodnightServersSettings" style="margin-top: 0; margin-bottom: 30px; font-size: 22px; color: #fff; text-decoration: underline;">' +
        'Goodnight Servers\' Settings' +
    '</div>' +
    '<div style="margin: 0">' +
        '<div style="width: 100%; margin: 0; margin-top: 0; margin-bottom: 10px; text-align: center;">' +
            '<span style="position: absolute; left: 60px; color: #fff;">' +
                'KeyCode:' +
            '</span>' +
            '<input id="keyCodeInput" style="width: 150px; padding-left: 2px; padding-right: 2px;" value="' + this.keyCode + '" />' + 
        '</div>' +
        '<div style="width: 100%; margin: 0; margin-top: 10px; margin-bottom: 0; text-align: center;">' +
            '<span style="position: absolute; left: 60px; color: #fff;">' +
                'Transition Time:' +
            '</span>' +
            '<input id="transitionTimeInput" style="width: 150px; padding-left: 2px; padding-right: 2px;" value="' + this.transitionTime + '" />' + 
        '</div>' +
        '<div style="width: 100%; margin: 0; margin-top: 10px; margin-bottom: 0; text-align: center;">' +
            '<span style="position: absolute; left: 60px; color: #fff;">' +
                'Should Toggle:' +
            '</span>' +
            '<select id="shouldToggleInput" style="width: 150px; padding-top: 2px; padding-bottom: 2px;">' +
                '<option value="Held">Held</option>' +
                '<option value="Toggle">Toggle</option>' +
            '</select>' +
        '</div>' +
    '</div>' +
    '<div style="text-align: center; margin-top: 25px; margin-bottom: 25px;">' +
        '<button style="width: 100px; line-height: 26px;" onClick="goodnightServers.prototype.applySettings()">Save</button>' +
    '</div>' +
    '<div style="width: 100%; text-align: center; margin-top: 0;">' +
        '<a href="https://bitbucket.org/GeoDoX/">GeoDoX on BitBucket</a>' +
    '</div>' +
    '<script>' +
        'if (!goodnightServers.prototype.shouldToggle)' +
            '$("shouldToggleInput").val("Held");' +
        'else' +
            '$("shouldToggleInput").val("Toggle");' +
    '</script>';

    return settingsPanelHTML;
}

goodnightServers.prototype.getName = function()
{
    return this.name;
}

goodnightServers.prototype.getDescription = function()
{
    return 'Tucks the server list into bed until the TAB (default) is held. Can be changed to toggle in settings.';
}

goodnightServers.prototype.getVersion = function()
{
    return "1.0.0";
}

goodnightServers.prototype.getAuthor = function()
{
    return 'GeoDoX, with help from HansAnonymous';
}

// Unused Functions
goodnightServers.prototype.load = function() {}
goodnightServers.prototype.unload = function() {}
goodnightServers.prototype.onSwitch = function() {}
goodnightServers.prototype.onMessage = function() {}
goodnightServers.prototype.observer = function() {}

// Helpful functions
goodnightServers.prototype.namespaceEvent = function(e, namespace)
{
    return e + "." + namespace;
}

goodnightServers.prototype.parseBoolean = function(value, trueValue = 'true', falseValue = 'false', ignoreCase = true)
{
    if (ignoreCase)
    {
        value = value.toLowerCase();
        trueValue = trueValue.toLowerCase();
        falseValue = falseValue.toLowerCase();
    }

    if (value === trueValue)
        return true;
    else if (value === falseValue)
        return false;
    else
        throw new Error("Value was neither the equivalent of the true value or the false value.");
}