# **GeoDoX's Better Discord Plugins** #
Goodnight Servers (goodnightServers.plugin.js) - Tucks the server list into bed until the TAB (default) is held. Can be changed to toggle in settings.

Better Transparency Patcher - Patches your Discord installation to allow for transparent themes. Patches/Unpatches according to whether the plugin is enabled/disabled respectively.<br><br>Based off of Holly\'s Transparency Patcher.